# Welcome to Samples 2D

This code computes samples in a 2D feature space.
This code considers several classification scenarios, referred as examples.

## Dependencies
+ python
+ numpy
+ matplotlib

## Download
git clone https://michael_villamizar@bitbucket.org/michael_villamizar/code_samples_2d.git

## Usage
#### Run demo file:
```python
python demo.py
```
#### Code example:
```python
import samples_2d as dataset                                                    

# Number of positive and negative samples.
num_pos_samples = 1000
num_neg_samples = 1000

# Samples for scenario example 2.
samples, labels, num_classes = dataset.fun_example_2(num_pos_samples, num_neg_samples)

# Show samples.
dataset.fun_show_samples(samples, labels, num_classes)
```

## Examples
The following two-class examples have been implemented.

#### Example 1:
![Example 1](./imgs/example_1.png)
#### Example 2:
![Example 2](./imgs/example_2.png)
#### Example 3:
![Example 3](./imgs/example_3.png)

## Authors
**Michael Villamizar**
[[https://www.idiap.ch/mvillamizar/]](https://www.idiap.ch/~mvillamizar/).
Idiap Research Institute.
Switzerland - 2017.
