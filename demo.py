import samples_2d as dataset

# Message.
print('Welcome to Samples 2D')
print('Michael Villamizar\nIdiap Research Institute\n2017')

# Samples for scenario example 1.
samples, labels, num_classes = dataset.fun_example_1()

# Show samples.
dataset.fun_show_samples(samples, labels, num_classes)
